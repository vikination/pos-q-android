package com.codepolitan.poscodepolitan

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import kotlinx.android.synthetic.main.activity_login.*

class LoginActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_login)

        button_login.setOnClickListener {
            // Fungsi untuk pindah ke MainActivity
            startActivity(Intent(this@LoginActivity, MainActivity::class.java))
        }
    }
}
