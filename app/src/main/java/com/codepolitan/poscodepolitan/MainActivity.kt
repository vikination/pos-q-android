package com.codepolitan.poscodepolitan

import android.Manifest
import android.content.Intent
import android.os.Bundle
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import com.karumi.dexter.Dexter
import com.karumi.dexter.PermissionToken
import com.karumi.dexter.listener.PermissionDeniedResponse
import com.karumi.dexter.listener.PermissionGrantedResponse
import com.karumi.dexter.listener.PermissionRequest
import com.karumi.dexter.listener.single.PermissionListener
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        button_scan.setOnClickListener {
            Dexter.withActivity(this)
                .withPermission(Manifest.permission.CAMERA)
                .withListener(object :PermissionListener{
                    override fun onPermissionGranted(response: PermissionGrantedResponse?) {
                        startActivity(Intent(this@MainActivity, ScannerBarcodeActivity::class.java))
                    }

                    override fun onPermissionRationaleShouldBeShown(
                        permission: PermissionRequest?,
                        token: PermissionToken?
                    ) {
                        token?.continuePermissionRequest()
                    }

                    override fun onPermissionDenied(response: PermissionDeniedResponse?) {
                        Toast.makeText(this@MainActivity, "response :${response?.permissionName}", Toast.LENGTH_SHORT).show()
                    }

                }).check()

        }

        button_transaction.setOnClickListener {
            startActivity(Intent(this@MainActivity, DaftarTransaksiActivity::class.java))
        }
    }
}
