package com.codepolitan.poscodepolitan

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import androidx.recyclerview.widget.DividerItemDecoration
import androidx.recyclerview.widget.LinearLayoutManager
import kotlinx.android.synthetic.main.activity_daftar_transaksi.*

class DaftarTransaksiActivity : AppCompatActivity() {

    lateinit var adapter :DaftarTransaksiAdapter

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_daftar_transaksi)

        toolbar_daftartransaksi.setOnClickListener { finish() }

        initList()

        adapter.updateDataTransaksi(getDataDummy())

        // aksi button tambah transaksi
        button_tambahtransaksi.setOnClickListener {
            startActivity(Intent(this@DaftarTransaksiActivity, TambahTransaksiActivity::class.java))
        }
    }

    private fun initList(){
        adapter = DaftarTransaksiAdapter(this)
        list_daftartransaksi.adapter = adapter
        list_daftartransaksi.layoutManager = LinearLayoutManager(this)
        list_daftartransaksi.addItemDecoration(DividerItemDecoration(this, DividerItemDecoration.VERTICAL))
    }

    private fun getDataDummy() :ArrayList<DataTransaksi>{
        return arrayListOf(
            DataTransaksi("Rabu, 22-11-2020", "Rp 200.000"),
            DataTransaksi("Rabu, 22-11-2020", "Rp 200.000"),
            DataTransaksi("Rabu, 22-11-2020", "Rp 200.000"),
            DataTransaksi("Rabu, 22-11-2020", "Rp 200.000"),
            DataTransaksi("Rabu, 22-11-2020", "Rp 200.000"),
            DataTransaksi("Rabu, 22-11-2020", "Rp 200.000"),
            DataTransaksi("Rabu, 22-11-2020", "Rp 200.000"),
            DataTransaksi("Rabu, 22-11-2020", "Rp 200.000"),
            DataTransaksi("Rabu, 22-11-2020", "Rp 200.000")
        )
    }
}
