package com.codepolitan.poscodepolitan

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView

class DaftarTransaksiAdapter(var context :Context) :
    RecyclerView.Adapter<DaftarTransaksiAdapter.DataTransaksiViewHolder>() {

    var dataTransaksi = arrayListOf<DataTransaksi>()

    inner class DataTransaksiViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        private var dateTextView = itemView.findViewById<TextView>(R.id.date_datatransaksi)
        private var totalTextView = itemView.findViewById<TextView>(R.id.total_datatransaksi)

        fun bind(dataTransaksi: DataTransaksi){
            dateTextView.text = dataTransaksi.date
            totalTextView.text = dataTransaksi.total
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): DataTransaksiViewHolder {
        val view = LayoutInflater.from(context).inflate(R.layout.row_daftar_transaksi, parent, false)
        return DataTransaksiViewHolder(view)
    }

    override fun getItemCount(): Int = dataTransaksi.size

    override fun onBindViewHolder(holder: DataTransaksiViewHolder, position: Int) {
        holder.bind(dataTransaksi[position])
    }

    public fun updateDataTransaksi(dataTransaksi: ArrayList<DataTransaksi>){
        this.dataTransaksi = dataTransaksi
        notifyDataSetChanged()
    }
}