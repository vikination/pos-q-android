package com.codepolitan.poscodepolitan

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import androidx.appcompat.app.AlertDialog
import com.google.zxing.Result
import kotlinx.android.synthetic.main.activity_scanner_barcode.*
import me.dm7.barcodescanner.zxing.ZXingScannerView

class ScannerBarcodeActivity : AppCompatActivity(), ZXingScannerView.ResultHandler {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_scanner_barcode)

        toolbar_scanner.setNavigationOnClickListener { finish() }
    }

    override fun onResume() {
        super.onResume()
        barcodescanner_view.setResultHandler(this)
        barcodescanner_view.startCamera()
    }

    override fun onStop() {
        super.onStop()
        barcodescanner_view.stopCamera()
    }

    override fun handleResult(rawResult: Result?) {
        AlertDialog.Builder(this)
            .setMessage("result scan barcode : ${rawResult?.text}")
            .setPositiveButton("OK"){dialog, _ ->
                dialog.dismiss()
                barcodescanner_view.resumeCameraPreview(this)
            }.create().show()
    }
}
