package com.codepolitan.poscodepolitan

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.os.Handler

class SplashScreenActivity : AppCompatActivity() {

    companion object{
        const val DELAY = 3000 // 3 detik
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_splash_screen)

        // delay 3 detik kemudian pindah screen ke LoginActivity

        Handler().postDelayed({ // block pindah screen
            val intent = Intent(this@SplashScreenActivity, LoginActivity::class.java)
            startActivity(intent)
            finish()
        }, DELAY.toLong())
    }
}
